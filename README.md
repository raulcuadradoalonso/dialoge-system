# Dialog System

Simple dialog system for Unity. You can check how it works [here](https://raulcuadradoalonso.com/dialog-system/).

I'm currently working on fixing some bugs and adding features such as an emoji system (like in Yarn Spinner) that trigger custom animations for the character when they are read.