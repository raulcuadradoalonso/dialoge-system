﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTriggerInfoButton : DialogInfoButton
{
    SpriteRenderer m_infoButtonSpriteRenderer;

    void Awake()
    {
        m_infoButtonSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    public override void ShowInfoButton()
    {
        m_infoButtonSpriteRenderer.enabled = true;
    }

    public override void HideInfoButton()
    {
        m_infoButtonSpriteRenderer.enabled = false;
    }

    public override void SetImagePosition(float x, float y)
    {
        //Should this take z as parameter too? Fixed distance vs fixed size?
    }

    public override void UpdateSprite(Sprite spr)
    {
        m_infoButtonSpriteRenderer.sprite = spr;
    }
}
