﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class DialogNodeTools
{
    public List<DialogNode> splitText(string currentString, DialogBox dialogBox, float lineWidth = 0, bool debug = false)
    {
        Text currentTextBox = dialogBox.DialogText;

        if (lineWidth == 0)
        {
            lineWidth = dialogBox.DEFAULT_TEXT_WIDTH;
        }

        StringBuilder sb = new StringBuilder();

        TextGenerator textGen = new TextGenerator();
        List<DialogNode> stringNodes = new List<DialogNode>();
        int currentStringWidth = 0;
        int lastSpaceIndex = 0;
        int widthBeforeLastSpaceIndex = 0;

        //Before the string is split in different lines, it is checked to remove duplicated spaces
        currentString = removeDuplicatedSpaces(currentString);

        //We will use a few times the width of ' ' so we store it now in a variable
        int currentSpaceWidth = (int)textGen.GetPreferredWidth(" ", currentTextBox.GetGenerationSettings(currentTextBox.rectTransform.rect.size));

        sb.AppendLine("Starting spit loop.");
        for (int i = 0; i < currentString.Length; i++)
        {
            sb.AppendLine("Loop number: " + i);
            sb.AppendLine("Character: " + currentString[i]);
            sb.AppendLine(" - Current string width: " + currentStringWidth);

            int auxWidth = (int)textGen.GetPreferredWidth(currentString[i].ToString(), currentTextBox.GetGenerationSettings(currentTextBox.rectTransform.rect.size));

            sb.AppendLine(" - Character glyph width: " + auxWidth);

            if (currentString[i] == ' ') //To avoid space duplications
            {
                lastSpaceIndex = i;
                widthBeforeLastSpaceIndex = currentStringWidth;
            }

            //Now we can start checking if the character fits into the current line
            //1. If it fits
            if (currentStringWidth + auxWidth <= lineWidth)
            {
                currentStringWidth += auxWidth;

                //We check here the end of the string because the other conditions handle cases where there are words remaining
                if (i == currentString.Length - 1)
                {
                    sb.AppendLine(" - String split: " + currentString.Substring(0, i + 1));
                    sb.AppendLine(" - String width: " + currentStringWidth);

                    sb.AppendLine(" - End of the loop.");
                    //We check again if the last character is a space to remove it
                    int auxLastIndex = i + 1;
                    if (currentString[i] == ' ')
                    {
                        auxLastIndex = i;
                        currentStringWidth -= currentSpaceWidth;
                    }
                    DialogNode sn = new DialogNode(currentString.Substring(0, auxLastIndex), currentStringWidth);
                    stringNodes.Add(sn);
                }
            }
            //2. If the character that does not fit is part of a word, we need to split the string before that word
            else if (currentString[i] != ' ')
            {
                sb.AppendLine("<b> - The character doesn't fit and is part of a word.</b>");
                sb.AppendLine("    - String split: " + currentString.Substring(0, lastSpaceIndex));
                sb.AppendLine("    - String width: " + widthBeforeLastSpaceIndex);

                DialogNode sn = new DialogNode(currentString.Substring(0, lastSpaceIndex), widthBeforeLastSpaceIndex);
                stringNodes.Add(sn);
                //We reset the for loop to begin after the split again    
                //Lastspaceindex + 1 to remove the space since we don't want the next line to begin with an space
                currentString = currentString.Substring(lastSpaceIndex + 1);
                currentStringWidth = 0;
                //This is to avoid the i++ to make the loop to start again in i = 1 and not in i = 0
                i = -1;

                continue;
            }
            //3. This means that we can split the line because the last character of the line is a space (so the next line will begin with a new word) 
            //We delete that space because we don't need it
            else
            {
                sb.AppendLine("<b> - The current character does not fit in the current line but it's not part of a word so we can split here.</b>");
                //Using i instead of i + 1 removes the last space from the string
                DialogNode sn = new DialogNode(currentString.Substring(0, i), currentStringWidth);
                //So we need to update the current string width without the space
                currentStringWidth -= currentSpaceWidth;
                //We reset the for loop to begin after the split again
                if (i < currentString.Length - 1)
                {
                    sb.AppendLine(" - String split: " + currentString.Substring(0, i));
                    sb.AppendLine(" - String width: " + currentStringWidth);
                    //We need to check if the character that does not fit is an space (to remove it) or not
                    if (currentString[i] != ' ')
                    {
                        sb.AppendLine(" - Splitting. String remaining: " + currentString.Substring(i));
                        currentString = currentString.Substring(i);
                    }
                    else
                    {
                        sb.AppendLine(" - Splitting. String remaining: " + currentString.Substring(i + 1));
                        currentString = currentString.Substring(i + 1);
                    }
                    stringNodes.Add(sn);
                    //Reset the loop
                    i = -1;
                    currentStringWidth = 0;
                }
                continue;
            }            
        }

        if (debug) { Debug.Log(sb.ToString()); }
        return stringNodes;
    }

    string removeDuplicatedSpaces(string str)
    {
        if (str.Length > 1)
        {
            List<char> auxCharList = new List<char>(str.ToCharArray());

            for (int i = 1; i < auxCharList.Count; i++)
            {
                if (auxCharList[i] == ' ' && auxCharList[i - 1] == ' ')
                {
                    auxCharList.RemoveAt(i);
                    i -= 1;
                }
            }

            if (auxCharList[auxCharList.Count - 1] == ' ')
            {
                auxCharList.RemoveAt(auxCharList.Count - 1);
            }

            return string.Join("", auxCharList);
        }        

        //Check if the first character of the string is an space
        if (str[0] == ' ')
        {
            str = str.Substring(1);
        }

        return str;
    }

    public DialogBox FindCharacter(ref DialogBox[] characters, string characterName)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            if (characters[i].CharacterName == characterName)
            {
                return characters[i];
            }
        }

        return null;
    }  

    public void CheckForSpecialCommands(string currentDialog)
    {

    }
}