﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class DialogTrigger : MonoBehaviour
{
    #region Custom Editor Variables
#if UNITY_EDITOR

    public string FileName = "";
#endif
    #endregion

    public enum INTERACTION_TYPES
    {
        ONTRIGGERENTER,
        ONTRIGGEREXIT,
        ONPLAYERINTERACTION,
        ONSTART
    }

    public INTERACTION_TYPES m_currentInteractionType;

    System.Action m_afterInteractionFunction = null;

    bool m_debug = false;    

    //Dialog tools
    DialogManager m_dialogManager;
    DialogTriggerInfoButton m_dialogInfoButton;
    DialogNodeTools m_dialogNodeTools;

    //Status variables
    bool m_dialogueFinished = false;
    bool m_interactionEnabled = false;
    bool m_triggerFinished = false;
    public bool m_keepOrderBool = false; //If this is true, conversations will be considered in the list order

    int m_currentConversationIndex = 0;
    string m_lastCharacter = "";

    IReadOnlyList<Conversation> m_thisTriggerConversations;
    public DialogBox[] m_thisTriggerCharacters;

#region ReadOnly Variables
    public bool DialogueFinished
    { get { return m_dialogueFinished; } }

    public bool TriggerFinished
    { get { return m_triggerFinished; } }
#endregion

    void Start()
    {
        m_dialogManager = GameObject.Find("Dialog Manager").GetComponent<DialogManager>();
        m_dialogNodeTools = new DialogNodeTools();
        m_dialogInfoButton = GetComponentInChildren<DialogTriggerInfoButton>();

        m_thisTriggerConversations = m_dialogManager.GetConversations(FileName).AllConversations;

        if (m_debug) { debugConversationsFile(); }

        if (m_currentInteractionType == INTERACTION_TYPES.ONSTART)
        {
            checkInteraction();
        }

        if (m_thisTriggerConversations.Count == 0)
        {
            Debug.Log("Trigger " + gameObject.name + " has no conversations!");
            enabled = false;
        }
    }

    void debugConversationsFile()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("Conversations found for trigger: " + gameObject.name + " Number of conversations: " + m_thisTriggerConversations.Count);
        foreach (Conversation conv in m_thisTriggerConversations)
        {
            sb.AppendLine("Conversation: " + conv.Title);

            foreach (Dialog d in conv.Dialogs)
            {
                sb.AppendLine(d.CharacterName + ": " + d.DialogText);
            }
        }

        Debug.Log(sb.ToString());
    }

    void Update()
    {
        //Assuming that the conditions of the current conversations can't be false once the player is inside the trigger
        if (m_interactionEnabled)
        {
            ShowInteraction();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!m_dialogManager.IsInteracting() && !m_triggerFinished && findSuitableConversation())
        {
            if (other.tag == "Player")
            {
                switch (m_currentInteractionType)
                {
                    case INTERACTION_TYPES.ONTRIGGERENTER:
                        checkInteraction();
                        break;
                    case INTERACTION_TYPES.ONPLAYERINTERACTION:
                        checkInteraction(true);
                        break;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            switch (m_currentInteractionType)
            {
                case INTERACTION_TYPES.ONTRIGGEREXIT:
                    checkInteraction();
                    break;
                case INTERACTION_TYPES.ONPLAYERINTERACTION:
                    m_dialogInfoButton.HideInfoButton();
                    m_interactionEnabled = false;
                    break;
            }
        }
    }

    void checkInteraction(bool playerHasToInteract = false)
    {
        if (!m_dialogManager.IsInteracting() && !m_triggerFinished && findSuitableConversation())
        {
            if (!playerHasToInteract)
            {
                StartCoroutine(interaction());
            }
            else
            {
                m_dialogInfoButton.UpdateSprite(m_dialogManager.m_dialogInfoButtonSprite);
                m_dialogInfoButton.ShowInfoButton();
                m_interactionEnabled = true;
            }
        }
    }

    void ShowInteraction()
    {
        m_dialogInfoButton.ShowInfoButton();

        //TO DO: Generalize input so the user can decide in the inspector the button
        if (Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Space)) //Button1 is B in Xbox Controllers
        {
            m_dialogInfoButton.HideInfoButton();

            StartCoroutine(interaction());
        }
    }

    IEnumerator interaction()
    {
        StringBuilder sb = new StringBuilder();
        DialogBox currentDialogBox = m_dialogManager.DefaultDialogBox;

        sb.AppendLine("Interaction flow debug of trigger: " + gameObject.name);        

        List<Dialog> currentConversation = m_thisTriggerConversations[m_currentConversationIndex].Dialogs;
        int currentDialogIndex = 0;

        if (currentConversation.Count == 0) { Debug.Log("No dialogs in this conversation: " + m_thisTriggerConversations[m_currentConversationIndex].Title); }

        //Loop until we reach the end of the conversation
        while (currentDialogIndex < currentConversation.Count)
        {
            string characterName = currentConversation[currentDialogIndex].CharacterName;
            string nextDialog = currentConversation[currentDialogIndex].DialogText;

            m_dialogNodeTools.CheckForSpecialCommands(nextDialog);

            currentDialogBox = m_dialogNodeTools.FindCharacter(ref m_thisTriggerCharacters, characterName);

            bool nextDialogIsFromSameCharacter = checkIfNextDialogIsFromSameCharacter(currentConversation, currentDialogIndex);
            bool characterStartsToTalk;

            if (currentDialogBox.CharacterName == m_lastCharacter)
            {
                characterStartsToTalk = false;
            }
            else
            {
                characterStartsToTalk = true;
                m_lastCharacter = currentDialogBox.CharacterName;
            }

            //Send dialogue                
            m_dialogManager.StartInteraction(nextDialog, currentDialogBox, characterStartsToTalk, nextDialogIsFromSameCharacter);

            while (m_dialogManager.IsShowingDialogue())
            {
                yield return null;
            }

            currentDialogIndex++;
        }

        m_dialogManager.WaitUntilDialogIsHiddenToAllowNewInteraction(currentDialogBox);
        m_lastCharacter = "";

        /*
        //TO DO: allow afterInteractionFunction after each conversation if desired... How?
        //Currently ontly when all the conversations of the trigger are done

        if (ThisConversationHasAfterInteractionFunction && m_afterInteractionFunction != null)
        {
            m_afterInteractionFunction();
        }
        */

        updateTriggerStatus();

        if (m_debug) { Debug.Log(sb.ToString()); }

        yield return null;
    }

    bool findSuitableConversation()
    {
        if (!m_keepOrderBool)
        { 
            List<int> suitableConversationsIndices = new List<int>();
            for (int i = 0; i < m_thisTriggerConversations.Count; i++)
            {
                if (allConditionsAreTrue(i))
                {
                    suitableConversationsIndices.Add(i);
                }
            }

            if (suitableConversationsIndices.Count == 0)
            {
                return false;
            }
            else
            {
                m_currentConversationIndex = (suitableConversationsIndices.Count == 1) ? suitableConversationsIndices[0] : Random.Range(0, suitableConversationsIndices.Count);
                return true;
            }
        }

        return allConditionsAreTrue(m_currentConversationIndex);
    }

    void updateTriggerStatus()
    {
        //TO DO: Check if this conversation can be repeated
        m_thisTriggerConversations[m_currentConversationIndex].ConversationDone = true;
        m_currentConversationIndex++;

        if (m_currentConversationIndex > m_thisTriggerConversations.Count - 1)
        {
            m_triggerFinished = true;            
        }
    }

    bool checkIfNextDialogIsFromSameCharacter(List<Dialog> currentConversation, int currentDialogIndex)
    {
        if (currentDialogIndex + 1 < currentConversation.Count)
        {
            if (currentConversation[currentDialogIndex].CharacterName == currentConversation[currentDialogIndex + 1].CharacterName)
            {
                return true;
            }
        }

        return false;
    }

    bool allConditionsAreTrue(int index)
    {
        Conversation conv = m_thisTriggerConversations[index];
        return !conv.ConversationDone && !conv.ConditionsValues.Contains(false);
    }

    public List<string> GetCharactersNames()
    {
        List<string> charNames = new List<string>();

        if (m_thisTriggerCharacters != null)
        {
            for (int i = 0; i < m_thisTriggerCharacters.Length; i++)
            {
                charNames.Add(m_thisTriggerCharacters[i].CharacterName);
            }
        }       

        return charNames;
    }

    public void SetAfterInteractionFunction(System.Action action)
    {
        m_afterInteractionFunction = action;
    }

    public void SetDebug(bool status)
    {
        m_debug = status;
    }

    public bool StopPlayerInThisConversation()
    {
        return m_thisTriggerConversations[m_currentConversationIndex].StopPlayer;
    }
}