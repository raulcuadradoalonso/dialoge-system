﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//Classes that will be used by the rest of the system (we need lists because dialogs can be modified)

[System.Serializable]
public class Conversations
{
    public Conversations(List<Conversation> ac)
    {
        AllConversations = ac;
    }

    public List<Conversation> AllConversations = new List<Conversation>();
}

[System.Serializable]
public class Conversation
{
    public bool StopPlayer = true;
    public bool ConversationDone = false;

    public string Title;
    public List<Dialog> Dialogs = new List<Dialog>();
    public List<string> ConditionsKeys = new List<string>();
    public List<bool> ConditionsValues = new List<bool>();

    public void AddCondition(string key, bool value)
    {
        ConditionsKeys.Add(key);
        ConditionsValues.Add(value);
    }

    public int ConditionsLength()
    {
        return ConditionsKeys.Count;
    }

    public string GetConditionKey(int index)
    {
        return ConditionsKeys[index];
    }

    public bool GetConditionValue(int index)
    {
        return ConditionsValues[index];
    }
    
    public void SetConditionKey(int index, string key)
    {
        ConditionsKeys[index] = key;
    }

    public void SetConditionValue(int index, bool value)
    {
        ConditionsValues[index] = value;
    }

    public bool FindConditionValue(string key)
    {
        int index = ConditionsKeys.IndexOf(key);
        return ConditionsValues[index];
    }

    public void RemoveAt(int index)
    {
        ConditionsValues.RemoveAt(index);
        ConditionsKeys.RemoveAt(index);
    }
}

[System.Serializable]
public class Dialog
{
    public string CharacterName;
    public string DialogText;
}