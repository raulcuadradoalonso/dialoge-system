﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DialogResizer
{
    public int GetMaxNumberOfLines(Text dialogText)
    {
        float height = dialogText.cachedTextGenerator.GetPreferredHeight("Sample", dialogText.GetGenerationSettings(dialogText.rectTransform.rect.size));
        Debug.Log(dialogText.rectTransform.rect.size);
        return (int) (dialogText.rectTransform.rect.height / height);
    }

    public Vector2 GetDialogTextBoxDimensions(ref DialogBox db, int numberOfLines, ref List<DialogNode> dialogNodes)
    {
        float targetTextWidth = findLongestString(dialogNodes.GetRange(0, numberOfLines));
        float targetTextHeight = getTextBoxLineHeight(db, numberOfLines);

        return new Vector2 (targetTextWidth, targetTextHeight);
    }

    //TextBox dimensions are received with the new size already
    //Modify this function if you want to get a different size for the Image of the dialog box
    public Vector2 GetDialogImageBoxDimensions(ref DialogBox db, Vector2 newTextBoxDimensions)
    {
        float newImageWidth = newTextBoxDimensions.x + db.DEFAULT_WIDTH_DIFFERENCE;
        float newImageHeight = newTextBoxDimensions.y + db.DEFAULT_HEIGHT_DIFFERENCE;

        return new Vector2(newImageWidth, newImageHeight);
    }

    float getTextBoxLineHeight(DialogBox db, int numberOfLines)
    {
        Vector2 extents = db.DialogText.rectTransform.rect.size;
        return db.DialogText.cachedTextGenerator.GetPreferredHeight(string.Concat(Enumerable.Repeat("\n", numberOfLines - 1)), db.DialogText.GetGenerationSettings(extents));
    }

    int findLongestString(List<DialogNode> stringNodes)
    {
        int max = 0;
        for (int i = 0; i < stringNodes.Count; i++)
        {
            if (stringNodes[i].stringWidth > max)
            {
                //Debug.Log("Looking for longest string, now: " + stringNodes[i].stringNode);
                //Debug.Log("Its width: " + stringNodes[i].stringWidth);
                max = stringNodes[i].stringWidth;
            }
        }
        return max;
    }

    long map(long x, long in_min, long in_max, long out_min, long out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
}
