﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class DialogBox : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] string m_characterName = "";
    [Tooltip("The character to which the dialog box will be attached to.")]
    [SerializeField] GameObject m_character = null;
    [Tooltip("If enabled, the dialog box will be resized regarding to the amount of text.")]
    [SerializeField] bool m_resize = false;
    [Tooltip("If enabled, the dialog box will follow the character position, you can apply an offset!")]
    [SerializeField] bool trackPosition = false;
    [Tooltip("Offset applied from the center of the character (in screen coordinates)")]
    [SerializeField] float X_OFFSET = 0;
    [Tooltip("Offset applied from the center of the character (in screen coordinates)")]
    [SerializeField] float Y_OFFSET = 0;    

    Image m_dialogImage = null;
    Text m_dialogText = null;

    float defaultImageWidth;
    float defaultImageHeight;
    float defaultTextWidth;
    float defaultTextHeight;

    float widthDifferenceBetweenImageAndTextBox;
    float heightDifferenceBetweenImageAndTextBox;

    DialogBoxInfoButton m_dialogInfoButton;

    #region ReadOnly getters
    public Image DialogImage
    { get { return m_dialogImage; } }

    public Text DialogText
    { get { return m_dialogText; } }

    public DialogBoxInfoButton InfoButton
    { get { return m_dialogInfoButton; } }

    public bool Resize
    { get { return m_resize; } }

    public string CharacterName
    { get { return m_characterName; } }

    public float DEFAULT_IMG_WIDTH
    { get { return defaultImageWidth; } }

    public float DEFAULT_IMG_HEIGHT
    { get { return defaultImageHeight; } }

    public float DEFAULT_TEXT_WIDTH
    { get { return defaultTextWidth; } }

    public float DEFAULT_TEXT_HEIGHT
    { get { return defaultTextHeight; } }

    public float DEFAULT_WIDTH_DIFFERENCE
    { get { return widthDifferenceBetweenImageAndTextBox; } }

    public float DEFAULT_HEIGHT_DIFFERENCE
    { get { return heightDifferenceBetweenImageAndTextBox; } }
    #endregion

    void Awake()
    {
        m_dialogImage = GetComponentInChildren<Image>();
        m_dialogText = GetComponentInChildren<Text>();
        m_dialogInfoButton = GetComponentInChildren<DialogBoxInfoButton>();

        setDefaultDimensions();

        hideDialog();
    }

    void Update()
    {
        if (trackPosition)
        {
            //TO DO: Track the position of the associated character
        }

        //TO DO: Update sprite of its own info button

        Vector3 currentDialogImagePosition = m_dialogImage.rectTransform.position;
        Vector2 currentDialogImageSize = m_dialogImage.rectTransform.rect.size;
        m_dialogInfoButton.SetImagePosition(currentDialogImagePosition.x + (currentDialogImageSize.x * 0.5f), currentDialogImagePosition.y - (currentDialogImageSize.y * 0.5f));
    }

    void setDefaultDimensions()
    {
        defaultImageWidth = m_dialogImage.rectTransform.rect.width;
        defaultImageHeight = m_dialogImage.rectTransform.rect.height;
        defaultTextWidth = m_dialogText.rectTransform.rect.width;
        defaultTextHeight = m_dialogText.rectTransform.rect.height;

        widthDifferenceBetweenImageAndTextBox = Mathf.Abs(defaultImageWidth - defaultTextWidth);
        heightDifferenceBetweenImageAndTextBox = Mathf.Abs(defaultImageHeight - defaultTextHeight);
    }

    void showDialog()
    {
        m_dialogImage.canvasRenderer.SetAlpha(1);
        m_dialogText.canvasRenderer.SetAlpha(1);
    }

    void hideDialog()
    {
        m_dialogImage.canvasRenderer.SetAlpha(0);
        m_dialogText.canvasRenderer.SetAlpha(0);
        m_dialogText.text = "";

        m_dialogImage.rectTransform.sizeDelta = new Vector2(defaultImageWidth, defaultImageHeight);
        m_dialogText.rectTransform.sizeDelta = new Vector2(defaultTextWidth, defaultTextHeight);
    }

    public void IntroAnimation(float transitionTime, IEnumerator callback)
    {
        showDialog();

        //Scale stuff
        m_dialogImage.transform.localScale = Vector3.zero;
        m_dialogText.transform.localScale = Vector3.zero;

        m_dialogImage.gameObject.Tween(null, Vector3.zero, Vector3.one, transitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            m_dialogText.transform.localScale = t.CurrentValue;
            m_dialogImage.transform.localScale = t.CurrentValue;
        }, (t) =>
        {
            StartCoroutine(callback);
        });
    }

    public void outroDialogueAnimation(float transitionTime)
    {
        Vector3 currentScale = m_dialogText.transform.localScale;

        m_dialogImage.gameObject.Tween(null, currentScale, Vector3.zero, transitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            m_dialogText.transform.localScale = t.CurrentValue;
            m_dialogImage.transform.localScale = t.CurrentValue;
        }, (t) =>
        {
            hideDialog();
        });
    }
}
