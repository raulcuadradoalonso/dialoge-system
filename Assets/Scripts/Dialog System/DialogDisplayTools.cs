﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using DigitalRuby.Tween;

public class DialogDisplayTools : MonoBehaviour
{ 
    public bool m_debugStringSplit = false;

    DialogManager m_dialogManager;
    DialogNodeTools m_dialogNodeTools;
    DialogResizer m_dialogResizer;

    float m_transitionTimeIO;
    float m_transitionTime;
    float m_typingSpeed;
    float m_fastTypingSpeed;
    
    //Dialog box dimension settings
    int m_currentHeight; //The text height will only be modified according to the current number of lines
    int m_defaultNumberOfLines; //Max number of lines, defined by the initial size of the dialog box (calculated in dialogResizer->GetMaxNumberOfLines())
    int m_currentNumberOfLines;

    //Conditions    
    bool m_showingText = false;
    bool m_isInteracting = false;
    bool resizingDialog = false; //For smooth transitions, we have to wait until the box is resized to show the next dialog

    #region ReadOnly variables
    public bool ShowingDialogText
    { get { return m_showingText; } }

    public bool IsInteracting
    { get { return m_isInteracting; } }
    #endregion

    void Start()
    {      
        //----- Setup of the dialogue system        
        m_dialogManager = GetComponent<DialogManager>();
        m_dialogNodeTools = new DialogNodeTools();
        m_dialogResizer = new DialogResizer();

        //----- Take variables from the manager
        m_typingSpeed = m_dialogManager.TypingSpeed;
        m_fastTypingSpeed = m_dialogManager.FastTypingSpeed;
        m_transitionTime = m_dialogManager.TransitionSpeed;
        m_transitionTimeIO = m_dialogManager.TransitionSpeedIO;

        //----- Settings for default dialog box
        m_defaultNumberOfLines = m_dialogResizer.GetMaxNumberOfLines(m_dialogManager.DefaultDialogBox.DialogText);
    }

    public void StartInteraction(string dialogue, DialogBox db, bool characterStartsToTalkNow, bool nextDialogIsFromSameCharacter)
    {
        //Disable player movement
        m_isInteracting = true;
        m_showingText = true;

        //Get some lines of text
        List<DialogNode> dialogNodes = m_dialogNodeTools.splitText(dialogue, db, 0, m_debugStringSplit);
        updateCurrentNumberOfLines(dialogNodes);

        db.DialogText.text = "";        

        if (characterStartsToTalkNow)
        {
            if (db.Resize)
            {
                Vector2 newTextBoxDimensions = m_dialogResizer.GetDialogTextBoxDimensions(ref db, m_currentNumberOfLines, ref dialogNodes);
                Vector2 newImageDimensions = m_dialogResizer.GetDialogImageBoxDimensions(ref db, newTextBoxDimensions);

                db.DialogText.rectTransform.sizeDelta = newTextBoxDimensions;
                db.DialogImage.rectTransform.sizeDelta = newImageDimensions;
            }

            db.IntroAnimation(m_transitionTimeIO, runDialogue(dialogNodes, db, nextDialogIsFromSameCharacter));
        }
        else
        {
            resizeDialogBoxWithSmoothTransition(db, dialogNodes);
            StartCoroutine(waitAndRunDialog(dialogNodes, db, nextDialogIsFromSameCharacter));
        }
    }

    IEnumerator waitAndRunDialog(List<DialogNode> dialogNodes, DialogBox db, bool nextDialogIsFromSameCharacter)
    {
        while (resizingDialog)
        {
            yield return null;
        }

        StartCoroutine(runDialogue(dialogNodes, db, nextDialogIsFromSameCharacter));
    }

    IEnumerator runDialogue(List<DialogNode> dialogNodes, DialogBox db, bool nextDialogIsFromSameCharacter)
    {
        Image currentDialogImage = db.DialogImage;
        Text currentDialogTextBox = db.DialogText;

        StringBuilder sb = new StringBuilder();

        int linesShowed = 0;        

        while (dialogNodes.Count > 0)
        {
            //This adds a line of text to the currentText and then removes the string from the list
            foreach (char c in dialogNodes[0].stringNode)
            {
                sb.Append(c);
                currentDialogTextBox.text = sb.ToString();
                if (m_dialogManager.CheckInputKeysForFastTyping() && m_dialogManager.AllowFastTyping)
                {
                    yield return new WaitForSeconds(1 / m_fastTypingSpeed);
                }
                else
                {
                    yield return new WaitForSeconds(1 / m_typingSpeed);
                }
            }

            linesShowed++;
            sb.AppendLine();
            dialogNodes.RemoveAt(0);
            
            if (linesShowed == m_currentNumberOfLines)
            {
                linesShowed = 0;
                updateCurrentNumberOfLines(dialogNodes);

                //If there are strings remaining to show
                if (m_currentNumberOfLines > 0)
                {
                    db.InfoButton.ShowInfoButton();

                    while (!Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.Joystick1Button1))
                    {

                        yield return null;
                    }                    

                    db.InfoButton.HideInfoButton();

                    currentDialogTextBox.text = "";
                    sb = new StringBuilder();
                    
                    if (db.Resize)
                    {
                        resizeDialogBoxWithSmoothTransition(db, dialogNodes);
                    }                   

                    while (resizingDialog) { yield return null; }
                }
            }
        }

        db.InfoButton.ShowInfoButton();

        while (!Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            yield return null;
        }

        db.InfoButton.HideInfoButton();

        m_showingText = false;
        if (!nextDialogIsFromSameCharacter) { db.outroDialogueAnimation(m_transitionTimeIO); }
    }

    void resizeDialogBoxWithSmoothTransition(DialogBox db, List<DialogNode> dialogNodes)
    {
        Vector2 newTextBoxDimensions = m_dialogResizer.GetDialogTextBoxDimensions(ref db, m_currentNumberOfLines, ref dialogNodes);
        Vector2 newImageBoxDimensions = m_dialogResizer.GetDialogImageBoxDimensions(ref db, newTextBoxDimensions);
        
        smoothTransition(db.DialogImage.rectTransform, newImageBoxDimensions.x, newImageBoxDimensions.y);
        smoothTransition(db.DialogText.rectTransform, newTextBoxDimensions.x, newTextBoxDimensions.y);
    }

    void smoothTransition(RectTransform currentRectTransform, float width_target, float height_target)
    {
        resizingDialog = true;

        Vector2 currentSize = currentRectTransform.sizeDelta;
        Vector2 targetSize = new Vector2(width_target, height_target);

        if (currentSize == targetSize) { return; }

        currentRectTransform.gameObject.Tween(null, currentSize, targetSize, m_transitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            currentRectTransform.sizeDelta = t.CurrentValue;
        }, (t) =>
        {
            //On complete
            resizingDialog = false;
        });       
    }    

    void updateCurrentNumberOfLines(List<DialogNode> dialogNodes)
    {
        if (dialogNodes.Count >= m_defaultNumberOfLines) { m_currentNumberOfLines = m_defaultNumberOfLines; }
        else { m_currentNumberOfLines = dialogNodes.Count; }
    }

    public IEnumerator WaitUntilDialogIsHiddenToAllowInteraction(DialogBox db)
    {
        while (db.DialogText.canvasRenderer.GetAlpha() > 0 || db.DialogImage.canvasRenderer.GetAlpha() > 0)
        {
            yield return null;
        }

        m_isInteracting = false;
    }    
}