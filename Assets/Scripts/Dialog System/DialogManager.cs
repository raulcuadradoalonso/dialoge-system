﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class DialogManager : MonoBehaviour
{
    [Header("References")]
    [Tooltip("This will be the main dialog box. Usually for the main character of the game.")]
    [SerializeField] DialogBox m_mainDialogBox = null;
    [Tooltip("InfoButton is a sprite that shows which button has to press the player to interact with a trigger. " +
        "Triggers will update the sprite of its info button using this one, so if" +
        "you want to change the info button sprite, just modify this variable.")]
    public Sprite m_dialogInfoButtonSprite;
    [Header("Animation settings")]
    [Tooltip("Time for intros and outros.")]
    [SerializeField] float m_transitionTimeIO = 0;
    [Tooltip("For transitions between different sizes of the dialog box.")]
    [SerializeField] float m_transitionTime = 0;
    [Tooltip("Speed: how many characters per second are displayed. 1/Speed.")]
    [SerializeField] float m_typingSpeed = 0;
    [Tooltip("Speed applied when the player presses space (you can change it through script).")]    
    [SerializeField] float m_fastTypingSpeed = 0;
    [SerializeField] bool m_allowFastTyping = false;
    public KeyCode[] FastTypingInputKeys;    

    #region ReadOnly Getters
    public DialogBox DefaultDialogBox
    { get { return m_mainDialogBox; } }

    public float TypingSpeed
    { get { return m_typingSpeed; } }

    public float FastTypingSpeed
    { get { return m_fastTypingSpeed; } }

    public float TransitionSpeedIO
    { get { return m_transitionTimeIO; } }

    public float TransitionSpeed
    { get { return m_transitionTime; } }

    public bool AllowFastTyping
    { get { return m_allowFastTyping; } }
    #endregion

    DialogDisplayTools m_dialogDisplay;

    void Awake()
    { 
        //To ensure that the DialogTriggers can find this object
        this.gameObject.name = "Dialog Manager";

        m_dialogDisplay = GetComponent<DialogDisplayTools>();
    }

    public void StartInteraction(string dialogue, DialogBox db, bool characterStartsToTalk, bool nextDialogIsFromSameCharacter)
    {
        m_dialogDisplay.StartInteraction(dialogue, db, characterStartsToTalk, nextDialogIsFromSameCharacter);
    }

    public void WaitUntilDialogIsHiddenToAllowNewInteraction(DialogBox db)
    {
        StartCoroutine(m_dialogDisplay.WaitUntilDialogIsHiddenToAllowInteraction(db));
    }

    public bool CheckInputKeysForFastTyping()
    {
        for (int i = 0; i < FastTypingInputKeys.Length; i++)
        {
            if (Input.GetKey(FastTypingInputKeys[i]))
            {
                return true;
            }
        }

        return false;
    }

    public bool IsShowingDialogue()
    {
        return m_dialogDisplay.ShowingDialogText;
    }

    public bool IsInteracting()
    {
        return m_dialogDisplay.IsInteracting;
    }

    public Conversations GetConversations(string path)
    {
        return DialogFileManager.LoadFile(path);
    }
}