﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogNode {

    public string stringNode;
    public int stringWidth;

    public DialogNode (string stringNode, int stringWidth)
    {
        this.stringNode = stringNode;
        this.stringWidth = stringWidth;
    }
}