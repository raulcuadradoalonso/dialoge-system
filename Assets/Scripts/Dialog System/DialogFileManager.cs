﻿using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class DialogFileManager
{
    //Default paths
    public const string DEFAULT_DIALOGS_PATH = "/Dialogs/";

    public static Conversations LoadFile(string path)
    {
        string filePath = Application.streamingAssetsPath + DEFAULT_DIALOGS_PATH + SceneManager.GetActiveScene().name + "/" + path;
        //Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            Conversations loadedData = JsonUtility.FromJson<Conversations>(dataAsJson);

            return loadedData;
        }
        else
        {
            Debug.LogError("File " + path + " not found!");
            Debug.LogError("Full path: " + filePath);
            return null;
        }
    }

    public static void SaveFile(string filename, string sceneName, Conversations f)
    {
        string actualPath = Application.streamingAssetsPath + DEFAULT_DIALOGS_PATH + sceneName;

        if (!Directory.Exists(actualPath))
        {
            Directory.CreateDirectory(actualPath);
        }

        actualPath += "/" + filename;
        string jsonFile = JsonUtility.ToJson(f);
        File.WriteAllText(actualPath, jsonFile);
    }
}