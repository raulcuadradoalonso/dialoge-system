﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBoxInfoButton : DialogInfoButton
{
    Image m_infoButtonImage;

    void Awake()
    {
        m_infoButtonImage = GetComponent<Image>();
    }

    public override void ShowInfoButton()
    {
        m_infoButtonImage.canvasRenderer.SetAlpha(1);
    }

    public override void HideInfoButton()
    {
        m_infoButtonImage.canvasRenderer.SetAlpha(0);
    }

    public override void SetImagePosition(float x, float y)
    {
        m_infoButtonImage.rectTransform.position = new Vector3(x, y, 0);
    }

    public override void UpdateSprite(Sprite spr)
    {
        m_infoButtonImage.sprite = spr;
    }
}
