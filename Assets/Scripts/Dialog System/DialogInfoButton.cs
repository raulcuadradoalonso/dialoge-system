﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class DialogInfoButton : MonoBehaviour
{

    void Start()
    {
        HideInfoButton();
    }

    public abstract void ShowInfoButton();

    public abstract void HideInfoButton();

    public abstract void UpdateSprite(Sprite spr);

    public abstract void SetImagePosition(float x, float y);
}