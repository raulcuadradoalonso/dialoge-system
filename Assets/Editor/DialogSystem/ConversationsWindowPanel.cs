﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class ConversationsWindowPanel
{
    ReorderableList m_reorderableList;
    List<bool> m_mainFoldoutStatus;
    List<bool> m_conditionsFoldoutStatus;
    List<List<bool>> m_conditionsRenamingStatus;

    string conditionToBeRenamed = "";

    float m_defaultLineSeparationBetweenLists = EditorGUIUtility.singleLineHeight * 1.5f;
    float m_defaultLineBetweenListParameters = EditorGUIUtility.singleLineHeight * 1.5f;

    float m_labelWidth;

    void enableList(List<Conversation> conversations)
    {
        m_reorderableList = new ReorderableList(conversations, typeof(Conversation), draggable: true, displayHeader: true, displayAddButton: true, displayRemoveButton: true);

        m_reorderableList.drawHeaderCallback = drawHeaderCallback;
        m_reorderableList.drawElementCallback = drawElementCallback;
        m_reorderableList.elementHeightCallback += elementHeightCallback;
        m_reorderableList.onAddCallback += onAddCallback;
        m_reorderableList.onRemoveCallback += onRemoveCallback;

        m_mainFoldoutStatus = Enumerable.Repeat(false, conversations.Count).ToList();
        m_conditionsFoldoutStatus = Enumerable.Repeat(false, conversations.Count).ToList();
        resetConditionsRenamingStatusLists(conversations);
    }

    /// <summary>
    /// Draws the header for the reorderable list
    /// </summary>
    void drawHeaderCallback(Rect rect)
    {
        EditorGUI.LabelField(rect, "Conversations");
    }

    /// <summary>
    /// This methods decides how to draw each element in the list
    /// </summary>
    void drawElementCallback(Rect rect, int index, bool isactive, bool isfocused)
    {
        List<Conversation> conversations = (List<Conversation>)m_reorderableList.list;
        rect.y += 2;

        drawListFoldouts(rect, index, conversations);
    }

    /// <summary>
    /// Calculates the height of a single element in the list.
    /// This is extremely useful when displaying list-items with nested data.
    /// </summary>
    float elementHeightCallback(int index)
    {
        float height = m_defaultLineSeparationBetweenLists;

        if (m_mainFoldoutStatus[index] == true)
        {
            //Text field for renaming the conversation
            height += m_defaultLineBetweenListParameters;
            //Title of the conditions foldout
            height += m_defaultLineBetweenListParameters;

            if (m_conditionsFoldoutStatus[index] == true)
            {
                //Stop player condition
                height += m_defaultLineBetweenListParameters;

                //Custom conditions
                List<Conversation> conversations = (List<Conversation>)m_reorderableList.list;
                Conversation c = conversations[index];
                height += m_defaultLineBetweenListParameters * c.ConditionsKeys.Count;

                //Buttons
                height += m_defaultLineBetweenListParameters;
            }
        }
        return height;
    }

    /// <summary>
    /// Defines how a new list element should be created and added to our list.
    /// </summary>
    void onAddCallback(ReorderableList list)
    {
        Conversation c = new Conversation();
        c.Title = "New Conversation";

        List<Conversation> conversations = (List<Conversation>)list.list;
        conversations.Add(c);

        m_mainFoldoutStatus.Add(false);
        m_conditionsFoldoutStatus.Add(false);

        resetConditionsRenamingStatusLists(conversations);
    }

    void onRemoveCallback(ReorderableList list)
    {
        List<Conversation> conversations = (List<Conversation>)list.list;
        conversations.RemoveAt(list.index);

        m_mainFoldoutStatus.RemoveAt(list.index);
        m_conditionsFoldoutStatus.RemoveAt(list.index);
        m_conditionsRenamingStatus.RemoveAt(list.index);
    }

    public void DoLayoutList(List<Conversation> conversations)
    {
        if (m_reorderableList == null)
        {
            enableList(conversations);
        }

        m_reorderableList.DoLayoutList();
    }

    #region Aux draw function
    void drawListFoldouts(Rect rect, int index, List<Conversation> conversations)
    {
        Rect auxRect = new Rect(rect);
        auxRect.x += 10;
        auxRect.width = 150;
        auxRect.height = EditorGUIUtility.singleLineHeight;

        m_mainFoldoutStatus[index] = EditorGUI.Foldout(auxRect, m_mainFoldoutStatus[index], conversations[index].Title);

        if (m_mainFoldoutStatus[index] == true)
        {
            setCurrentLabelWidth(40);
            auxRect.x += 20;
            auxRect.y += m_defaultLineBetweenListParameters;
            auxRect.width = Screen.width * 0.3f;

            conversations[index].Title = EditorGUI.TextField(auxRect, new GUIContent("Title"), conversations[index].Title);
            restoreLabelWidth();

            drawConditionsFoldout(auxRect, index, conversations);
        }
    }

    void drawConditionsFoldout(Rect rect, int conversationIndex, List<Conversation> conversations)
    {
        Conversation currentConversation = conversations[conversationIndex];

        rect.y += m_defaultLineBetweenListParameters;
        m_conditionsFoldoutStatus[conversationIndex] = EditorGUI.Foldout(rect, m_conditionsFoldoutStatus[conversationIndex], "Conditions");        

        if (m_conditionsFoldoutStatus[conversationIndex] == true)
        {
            rect.x += 20;
            rect.y += m_defaultLineBetweenListParameters;
            currentConversation.StopPlayer = EditorGUI.ToggleLeft(rect, new GUIContent("Stop player"), currentConversation.StopPlayer);

            //Custom conditions
            for (int conditionIndex = 0; conditionIndex < currentConversation.ConditionsLength(); conditionIndex++)
            {
                if (!m_conditionsRenamingStatus[conversationIndex][conditionIndex])
                {
                    //Conditions check boxes
                    rect.y += m_defaultLineBetweenListParameters;
                    bool check = EditorGUI.ToggleLeft(rect, currentConversation.GetConditionKey(conditionIndex), currentConversation.GetConditionValue(conditionIndex));
                    currentConversation.SetConditionValue(conditionIndex, check);

                    //Buttons
                    Rect auxRect = rect;
                    auxRect.x += auxRect.width;
                    auxRect.width = 70;

                    string currentKey = currentConversation.GetConditionKey(conditionIndex);
                    if (GUI.Button(auxRect, "Rename"))
                    {
                        resetConditionsRenamingStatusLists(conversations);
                        m_conditionsRenamingStatus[conversationIndex][conditionIndex] = true;
                        conditionToBeRenamed = currentConversation.GetConditionKey(conditionIndex);
                    }

                    auxRect.x += auxRect.width;
                    if (GUI.Button(auxRect, "Remove"))
                    {
                        currentConversation.RemoveAt(conditionIndex);
                        m_conditionsRenamingStatus[conversationIndex].RemoveAt(conditionIndex);
                    }

                }
                else
                {
                    rect.y += m_defaultLineBetweenListParameters;
                    conditionToBeRenamed = EditorGUI.TextField(rect, conditionToBeRenamed);
                    currentConversation.SetConditionKey(conditionIndex, conditionToBeRenamed);

                    Rect auxRect = rect;
                    auxRect.x += auxRect.width;
                    auxRect.width = 70;
                    if (GUI.Button(auxRect, "Confirm"))
                    {
                        m_conditionsRenamingStatus[conversationIndex][conditionIndex] = false;
                    }
                }
            }

            rect.y += m_defaultLineBetweenListParameters;
            rect.width = 50;

            if (GUI.Button(rect, "Add"))
            {
                currentConversation.AddCondition("New condition", true);
                m_conditionsRenamingStatus[conversationIndex].Add(false);
            }
        }        
    }
    #endregion

    #region Aux functions
    void resetConditionsRenamingStatusLists(List<Conversation> conversations)
    {
        m_conditionsRenamingStatus = new List<List<bool>>();
        for (int i = 0; i < conversations.Count; i++)
        {
            m_conditionsRenamingStatus.Add(Enumerable.Repeat(false, conversations[i].ConditionsLength()).ToList());
        }
    }

    void setCurrentLabelWidth(float value)
    {
        m_labelWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = value;
    }

    void restoreLabelWidth()
    {
        EditorGUIUtility.labelWidth = m_labelWidth;
    }
    #endregion
}