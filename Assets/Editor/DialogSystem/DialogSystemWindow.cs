﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class DialogSystemWindow : EditorWindow
{

    bool m_interfaceEnabled = false;
    GameObject m_currentTrigger;
    List<Conversation> m_currentConversations;
    List<string> m_conversationsTitles; //To display them in the popup      
    
    bool m_triggerAlreadySetUp = false;      

    DialogWindowPanel m_dialogWindowPanel;
    ConversationsWindowPanel m_conversationsWindowPanel;

    [MenuItem("Window/Dialog System")]
    public static void ShowWindow()
    {
        GetWindow<DialogSystemWindow>("Dialog System");
    }

    void OnEnable()
    {
        m_dialogWindowPanel = new DialogWindowPanel();
        m_conversationsWindowPanel = new ConversationsWindowPanel();
    }    

    #region Draw Section
    void OnGUI()
    {
        if (!m_interfaceEnabled)
        {
            m_dialogWindowPanel.DrawInfoText("Select a DialogTrigger to start creating dialogs!");
            return;
        }

        m_dialogWindowPanel.InitializeToolbarButtonsSkin();
        GUILayout.BeginHorizontal();
        m_dialogWindowPanel.DrawToggleButtons();
        GUILayout.EndHorizontal();

        //Section to modify dialgos from a seleted conversation
        if (!m_dialogWindowPanel.ReorderableListSection)
        {
            setUpConversationTitlesList();

            m_dialogWindowPanel.DrawConversationSelectorPanel(position, m_currentTrigger, m_currentConversations, m_conversationsTitles);
            m_dialogWindowPanel.DrawDialogModifierPanel(position, m_currentTrigger, m_currentConversations);
            m_dialogWindowPanel.DrawResizer(position);
        }
        //Section to modify the order of the conversations of a trigger and their conditions
        else
        {            
            m_conversationsWindowPanel.DoLayoutList(m_currentConversations);
        }

        m_dialogWindowPanel.ProcessEvents(Event.current, position);
        if (GUI.changed)
        {
            Repaint();
            saveChanges();
        }
    }

    void Update()
    {
        Repaint();
        checkToEnableWindowInterface();
    }
    #endregion

    #region window status
    void checkToEnableWindowInterface()
    {
        if (Selection.activeGameObject != null && Selection.activeGameObject != m_currentTrigger)
        {
            m_currentTrigger = Selection.activeGameObject;
            m_triggerAlreadySetUp = false;
            resetWindowInfo();
        }
        else if (Selection.activeGameObject != null)
        {
            m_interfaceEnabled = Selection.activeGameObject.GetComponent<DialogTrigger>() != null;
            if (m_interfaceEnabled && !m_triggerAlreadySetUp)
            {
                setUpTriggerInfo();
                m_triggerAlreadySetUp = true;
            }
        }
        else if (Selection.activeGameObject == null)
        {
            resetWindowInfo();
            m_currentTrigger = null;
            m_triggerAlreadySetUp = false;
        }
    }
    #endregion

    #region Aux functions
    void resetWindowInfo()
    {
        m_interfaceEnabled = false;
        m_dialogWindowPanel.ResetWindowInfo();
    }

    void setUpTriggerInfo()
    {
        m_currentTrigger = Selection.activeGameObject;

        string filePath = m_currentTrigger.GetComponent<DialogTrigger>().FileName;

        if (filePath == "")
        {
            m_currentConversations = new List<Conversation>();
            m_currentTrigger.GetComponent<DialogTrigger>().FileName = m_currentTrigger.name + "_" + m_currentTrigger.GetInstanceID() + ".json";
            saveChanges();
        }
        else
        {
            m_currentConversations = DialogFileManager.LoadFile(filePath).AllConversations;
        }

        setUpConversationTitlesList();

        m_dialogWindowPanel.SetCharactersOfThisTrigger(m_currentTrigger.GetComponent<DialogTrigger>().GetCharactersNames());
    }

    void setUpConversationTitlesList()
    {
        m_conversationsTitles = new List<string>();
        for (int i = 0; i < m_currentConversations.Count; i++)
        {
            m_conversationsTitles.Add(m_currentConversations[i].Title);
        }
    }

    void saveChanges()
    {
        if (m_currentConversations != null && m_currentTrigger != null)
        {
            DialogTrigger dt = m_currentTrigger.GetComponent<DialogTrigger>();

            if (dt != null)
            {
                DialogFileManager.SaveFile(dt.FileName, SceneManager.GetActiveScene().name, new Conversations(m_currentConversations));
            }
        }
    }
    #endregion
}