﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class DialogWindowPanel
{
    //-----GUI
    Vector2 m_conversationScroll;
    Vector2 m_dialogModifierScroll;

    //-----Resizer
    Rect m_conversationSelectorPanel;
    Rect m_dialogModifierPanel;
    Rect m_resizer;
    float m_sizeRatio = 0.7f;
    float m_resizerHeight = 5f;
    bool m_isResizing;
    GUIStyle m_resizerStyle;

    string m_conversationName = "New conversation";
    string m_currentDialog = "";

    int m_currentConversationIndex = 0; //Conversation selected in the popup
    int m_currentCharacterDialogIndex = 0; //Character name selected in the dialog modifier  
    int m_lastConversationIndex = 0;
    bool m_firstTimeTriggerWindow = true;

    List<string> m_charactersOfThisTrigger;
    Dialog m_dialogSelected = null;

    GUIStyle m_boxStyle;
    Texture2D m_boxDarkTexture;
    Texture2D m_boxBrightTexture;
    Texture2D m_boxSelectedTexture;

    //-----Buttons as a switch for the two windows
    GUIStyle m_toggleButtonStyleNormal = null;
    GUIStyle m_toggleButtonStyleToggled = null;
    bool m_reorderableListSection = false;

    #region ReadOnly variables
    public bool ReorderableListSection
    { get { return m_reorderableListSection; } }
    #endregion

    public DialogWindowPanel()
    {
        m_dialogSelected = null;

        m_resizerStyle = new GUIStyle();
        m_resizerStyle.normal.background = EditorGUIUtility.Load("icons/d_AvatarBlendBackground.png") as Texture2D;

        m_boxStyle = new GUIStyle();
        m_boxStyle.normal.textColor = new Color(0.7f, 0.7f, 0.7f);

        m_boxDarkTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/cn entrybackodd.png") as Texture2D;
        m_boxBrightTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/cnentrybackeven.png") as Texture2D;
        m_boxSelectedTexture = EditorGUIUtility.Load("builtin skins/darkskin/images/menuitemhover.png") as Texture2D;
    }

    public void DrawConversationSelectorPanel(Rect position, GameObject trigger, List<Conversation> currentConversations, List<string> conversationTitles)
    {
        GUIContent content = new GUIContent("Conversations");
        Vector2 toggleButtonsSize = m_toggleButtonStyleNormal.CalcSize(content);
        m_conversationSelectorPanel = new Rect(0, toggleButtonsSize.y, position.width, position.height * m_sizeRatio - toggleButtonsSize.y);

        GUILayout.BeginArea(m_conversationSelectorPanel);

        GUILayout.Label("Conversation selector", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        drawConversationOptionsButtons(currentConversations, conversationTitles);
        GUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Conversation:", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        m_conversationScroll = EditorGUILayout.BeginScrollView(m_conversationScroll);

        drawCurrentConversationDialogs(trigger, currentConversations);

        EditorGUILayout.EndScrollView();
        GUILayout.Space(10);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    public void DrawDialogModifierPanel(Rect position, GameObject trigger, List<Conversation> currentConversations)
    {
        m_dialogModifierPanel = new Rect(0, (position.height * m_sizeRatio) + m_resizerHeight, position.width, position.height * (1 - m_sizeRatio) - m_resizerHeight);
        keepCharactersNamesListUpdated(trigger);

        GUILayout.BeginArea(m_dialogModifierPanel);

        GUILayout.Label("Dialog modifier", EditorStyles.boldLabel);

        if (m_charactersOfThisTrigger.Count > 0 && currentConversations.Count > 0)
        {
            m_dialogModifierScroll = EditorGUILayout.BeginScrollView(m_dialogModifierScroll);

            if (m_dialogSelected != null)
            {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Character name: ", EditorStyles.boldLabel, GUILayout.MaxWidth(120));
                m_currentCharacterDialogIndex = EditorGUILayout.Popup(m_currentCharacterDialogIndex, m_charactersOfThisTrigger.ToArray(), GUILayout.MaxWidth(200));
                m_currentCharacterDialogIndex = (m_currentCharacterDialogIndex < 0) ? 0 : m_currentCharacterDialogIndex;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Dialog: ", EditorStyles.boldLabel, GUILayout.MaxWidth(120));
                m_currentDialog = EditorGUILayout.TextArea(m_currentDialog);
                //GUILayout.Space(10);
                GUILayout.EndHorizontal();

                m_currentCharacterDialogIndex = (m_currentCharacterDialogIndex < 0) ? 0 : m_currentCharacterDialogIndex;
                m_dialogSelected.CharacterName = m_charactersOfThisTrigger[m_currentCharacterDialogIndex];
                m_dialogSelected.DialogText = m_currentDialog;
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("New dialog", GUILayout.MaxWidth(120)))
            {
                List<Dialog> currentConversation = currentConversations[m_currentConversationIndex].Dialogs;

                int currentIndex = currentConversation.Count - 1;
                if (m_dialogSelected != null)
                {
                    currentIndex = currentConversation.IndexOf(m_dialogSelected);
                }

                Dialog newDialog = new Dialog();

                if (currentIndex > 0)
                {
                    newDialog.CharacterName = currentConversation[currentIndex - 1].CharacterName;
                }

                newDialog.DialogText = "Write a new dialog here!";
                m_currentDialog = newDialog.DialogText;
                currentConversation.Insert(currentIndex + 1, newDialog);

                m_dialogSelected = newDialog;
                GUI.FocusControl("");
            }
            if (m_dialogSelected != null && GUILayout.Button("Remove dialog", GUILayout.MaxWidth(120)))
            {
                List<Dialog> currentConversation = currentConversations[m_currentConversationIndex].Dialogs;
                int currentIndex = currentConversation.IndexOf(m_dialogSelected);
                currentConversation.RemoveAt(currentIndex);

                if (currentConversation.Count > 0)
                {
                    m_dialogSelected = (currentIndex == 0) ? currentConversation[0] : currentConversation[currentIndex - 1];

                    m_currentCharacterDialogIndex = m_charactersOfThisTrigger.IndexOf(m_dialogSelected.CharacterName);
                    m_currentDialog = m_dialogSelected.DialogText;
                }
                GUI.FocusControl("");
            }
            GUILayout.EndHorizontal();

            EditorGUILayout.EndScrollView();
        }
        else if (currentConversations.Count == 0)
        {
            DrawInfoText("Start creating a conversation!");
        }
        else if (m_charactersOfThisTrigger.Count == 0)
        {
            DrawInfoText("This trigger has no characters asigned!");
        }
        
        GUILayout.EndArea();
    }

    public void InitializeToolbarButtonsSkin()
    {
        if (m_toggleButtonStyleNormal == null)
        {
            m_toggleButtonStyleNormal = "Toolbar";
            m_toggleButtonStyleNormal.alignment = TextAnchor.MiddleCenter;
            m_toggleButtonStyleToggled = new GUIStyle(m_toggleButtonStyleNormal);
            m_toggleButtonStyleToggled.normal.background = m_toggleButtonStyleToggled.active.background;
        }
    }

    public void DrawToggleButtons()
    {
        if (GUILayout.Button("Conversations", m_reorderableListSection ? m_toggleButtonStyleToggled : m_toggleButtonStyleNormal))
        {
            m_reorderableListSection = true;
        }
        if (GUILayout.Button("Dialogs", !m_reorderableListSection ? m_toggleButtonStyleToggled : m_toggleButtonStyleNormal))
        {
            m_reorderableListSection = false;
        }
    }

    void drawConversationOptionsButtons(List<Conversation> currentConversations, List<string> conversationTitles)
    {
        m_currentConversationIndex = (m_currentConversationIndex >= conversationTitles.Count) ? conversationTitles.Count - 1 : m_currentConversationIndex;
        m_currentConversationIndex = EditorGUILayout.Popup(m_currentConversationIndex, conversationTitles.ToArray(), GUILayout.MaxWidth(200));
        m_currentConversationIndex = (m_currentConversationIndex < 0) ? 0 : m_currentConversationIndex;
        checkIfConversationHasChanged();
    }

    void drawCurrentConversationDialogs(GameObject trigger, List<Conversation> currentConversations)
    {
        keepCharactersNamesListUpdated(trigger);
        List<Dialog> currentConversation = (currentConversations.Count > 0) ? currentConversations[m_currentConversationIndex].Dialogs : new List<Dialog>();
        for (int i = 0; i < currentConversation.Count; i++)
        {
            Dialog currentDialog = currentConversation[i];
            string characterName = currentDialog.CharacterName;
            //Check if the character has been deleted from the trigger
            if (!m_charactersOfThisTrigger.Contains(characterName) && m_charactersOfThisTrigger.Count > 0)
            {
                currentDialog.CharacterName = m_charactersOfThisTrigger[0];
            }

            if (drawBox(characterName + ": " + currentDialog.DialogText, i % 2 != 0, currentDialog == m_dialogSelected))
            {
                m_dialogSelected = currentDialog;
                this.m_currentDialog = m_dialogSelected.DialogText;
                m_currentCharacterDialogIndex = m_charactersOfThisTrigger.IndexOf(m_dialogSelected.CharacterName);

                GUI.FocusControl("");
            }
        }
    }

    #region Resize stuff
    public void DrawResizer(Rect position)
    {
        m_resizer = new Rect(0, (position.height * m_sizeRatio) - m_resizerHeight, position.width, m_resizerHeight * 2);

        GUILayout.BeginArea(new Rect(m_resizer.position + (Vector2.up * m_resizerHeight), new Vector2(position.width, 2)), m_resizerStyle);
        GUILayout.EndArea();

        EditorGUIUtility.AddCursorRect(m_resizer, MouseCursor.ResizeVertical);
    }

    public void ProcessEvents(Event e, Rect position)
    {
        switch (e.type)
        {
            case EventType.MouseDown:

                if (e.button == 0 && m_resizer.Contains(e.mousePosition))
                {
                    m_isResizing = true;
                }
                else
                {

                }
                break;

            case EventType.MouseUp:
                m_isResizing = false;
                break;
        }

        resize(e, position);
    }

    void resize(Event e, Rect position)
    {
        if (m_isResizing)
        {
            m_sizeRatio = e.mousePosition.y / position.height;
        }
    }
    #endregion

    #region Aux draw functions
    public void DrawInfoText(string message)
    {
        GUILayout.FlexibleSpace();
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label(message, labelStyle);
        GUILayout.FlexibleSpace();
    }

    bool drawBox(string content, bool isOdd, bool isSelected)
    {
        if (isSelected)
        {
            m_boxStyle.normal.background = m_boxSelectedTexture;
        }
        else
        {
            if (isOdd)
            {
                m_boxStyle.normal.background = m_boxBrightTexture;
            }
            else
            {
                m_boxStyle.normal.background = m_boxDarkTexture;
            }
        }

        m_boxStyle.alignment = TextAnchor.MiddleLeft;
        m_boxStyle.padding = new RectOffset(5, 5, 5, 5);

        return GUILayout.Button(content, m_boxStyle, GUILayout.Height(30));
    }
    #endregion

    #region Aux general functions
    void keepCharactersNamesListUpdated(GameObject trigger)
    {
        if (trigger == null) { return; }
        if (trigger.GetComponent<DialogTrigger>() == null) { return; }

        m_charactersOfThisTrigger = trigger.GetComponent<DialogTrigger>().GetCharactersNames();

        if (m_currentCharacterDialogIndex >= m_charactersOfThisTrigger.Count)
        {
            m_currentCharacterDialogIndex = m_charactersOfThisTrigger.Count - 1;
        }
    }

    void checkIfConversationHasChanged()
    {
        if (m_firstTimeTriggerWindow)
        {
            m_lastConversationIndex = m_currentConversationIndex;
            m_firstTimeTriggerWindow = false;
        }
        else if (m_lastConversationIndex != m_currentConversationIndex)
        {
            m_dialogSelected = null;
            m_lastConversationIndex = m_currentConversationIndex;
        }
    }

    public void ResetWindowInfo()
    {
        m_firstTimeTriggerWindow = true;
        m_dialogSelected = null;
        m_conversationName = "New conversation";
        m_currentDialog = "";
        m_currentConversationIndex = 0;
        m_currentCharacterDialogIndex = 0;
    }

    public void SetCharactersOfThisTrigger(List<string> characters)
    {
        m_charactersOfThisTrigger = characters;
    }    
    #endregion
}
