﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DialogTrigger))]
public class DialogTriggerEditor : Editor
{
    GameObject m_dialogCanvas;

    int m_currentCharacterIndex = 0;
    SerializedProperty m_interactionType;

    void OnEnable()
    {
        m_interactionType = serializedObject.FindProperty("m_currentInteractionType");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (m_dialogCanvas == null)
        {
            m_dialogCanvas = GameObject.Find("DialogCanvas");
        }

        EditorGUILayout.LabelField("Settings");
        EditorGUILayout.BeginHorizontal();
        GUIContent popupContent = new GUIContent();
        popupContent.text = "Interaction type:";
        EditorGUILayout.PropertyField(m_interactionType, popupContent);
        EditorGUILayout.EndHorizontal();

        SerializedProperty keepOrderBool = serializedObject.FindProperty("m_keepOrderBool");
        GUIContent toggleContent = new GUIContent("Keep order", "If true, conversations will be triggered in order.");
        keepOrderBool.boolValue = EditorGUILayout.Toggle(toggleContent, keepOrderBool.boolValue);

        GUILayout.Label("Characters available: ", EditorStyles.boldLabel);
        DialogBox[] charactersInTheScene = getCharactersList();
        m_currentCharacterIndex = EditorGUILayout.Popup(m_currentCharacterIndex, getCharacterNameArray(charactersInTheScene), GUILayout.MaxWidth(200));

        SerializedProperty charactersList = serializedObject.FindProperty("m_thisTriggerCharacters");

        if (GUILayout.Button("Add character", GUILayout.MaxWidth(120)) && isValidCharacter(charactersInTheScene[m_currentCharacterIndex].CharacterName, charactersList))
        {
            int nextPosition = charactersList.arraySize;
            charactersList.InsertArrayElementAtIndex(nextPosition);
            charactersList.GetArrayElementAtIndex(nextPosition).objectReferenceValue = charactersInTheScene[m_currentCharacterIndex];
        }

        DisplayCharactersList(serializedObject.FindProperty("m_thisTriggerCharacters"));

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("File name: ", EditorStyles.boldLabel, GUILayout.MaxWidth(80)); 
        GUILayout.Label(serializedObject.FindProperty("FileName").stringValue, EditorStyles.boldLabel);
        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
    
    public static void DisplayCharactersList(SerializedProperty list)
    {
        EditorGUILayout.LabelField("Characters in this trigger: ", EditorStyles.boldLabel);
        EditorGUI.indentLevel += 1;
        for (int i = 0; i < list.arraySize; i++)
        {
            DialogBox aux = (DialogBox) list.GetArrayElementAtIndex(i).objectReferenceValue;
            EditorGUILayout.BeginHorizontal();
            string charName = (aux == null) ? "Null" : aux.CharacterName;
            EditorGUILayout.LabelField(charName);
            if (GUILayout.Button("Delete"))
            {
                //This check is done because when you remove an element from a serialized array, if it has no content is actually removed,
                //but if it has an object attached what it clears the reference, but not deletes the entire array element
                int oldSize = list.arraySize;
                list.DeleteArrayElementAtIndex(i);
                if (list.arraySize == oldSize)
                {
                    list.DeleteArrayElementAtIndex(i);
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUI.indentLevel -= 1;
    }

    DialogBox[] getCharactersList()
    {
        return m_dialogCanvas.GetComponentsInChildren<DialogBox>();
    }

    string[] getCharacterNameArray(DialogBox[] db)
    {
        string[] characterNames = new string[db.Length];
        for (int i = 0; i < db.Length; i++)
        {
            characterNames[i] = db[i].CharacterName;
        }

        return characterNames;
    }

    bool isValidCharacter(string name, SerializedProperty list)
    {
        for (int i = 0; i < list.arraySize; i++)
        {
            DialogBox aux = (DialogBox)list.GetArrayElementAtIndex(i).objectReferenceValue;
            if (aux.CharacterName == name)
            {
                return false;
            }
        }

        return true;
    }
}
